#ifndef _COOLDOWN_RELAY_H
#define _COOLDOWN_RELAY_H

#include "common.h"

namespace CooldownRelay
{
const millis_t DEFAULT_ON_BEFORE_COOLDOWN_MS = MINUTE;
const millis_t DEFAULT_COOLDOWN_MS = MINUTE;
const millis_t DEFAULT_CHANGE_THRESHOLD_MS = 50;

enum State {
  OFF = 0,
  ON,
  COOLDOWN
};

struct CooldownRelay {
  State state = OFF;
  millis_t change_threshold_ms = DEFAULT_CHANGE_THRESHOLD_MS;
  millis_t cooldown_ms = DEFAULT_COOLDOWN_MS;
  millis_t open_before_cooldown_ms = DEFAULT_ON_BEFORE_COOLDOWN_MS;
  millis_t open_since = 0;
  millis_t next_update = 0;
  static const size_t MAX_LABEL_LEN = 32;
  char label[MAX_LABEL_LEN] = "Relay";
  byte pin = -1;

  CooldownRelay(byte pin, char* label = NULL, millis_t change_threshold_ms = DEFAULT_CHANGE_THRESHOLD_MS,
                millis_t open_before_cooldown_ms = DEFAULT_ON_BEFORE_COOLDOWN_MS, millis_t cooldown_ms = DEFAULT_COOLDOWN_MS) {
    this->pin = pin;
    this->change_threshold_ms = change_threshold_ms;
    this->open_before_cooldown_ms = open_before_cooldown_ms;
    this->cooldown_ms = cooldown_ms;
    if(label)
      strncpy(this->label, label, MAX_LABEL_LEN);
    // RIAA
    pinMode(pin, OUTPUT);
  }

  ~CooldownRelay() {
    if ( state == ON )
      close();
  }

  void open() {
    if (state != ON)
    {
      log("%s: On", label);
      digitalWrite(pin, HIGH);
      state = ON;
      open_since = millis();
    }
  }

  bool is_open(void) {
    return state == ON;
  }

  void close() {
    if (state != OFF)
    {
      log("%s: Off", label);
      digitalWrite(pin, LOW);
      state = OFF;
    }
  }

  void cooldown(millis_t current_millis) {
    log("%s: Cooldown", label);
    close();
    state = COOLDOWN;
    // Take a minute to think about what you've done!
    next_update = current_millis + cooldown_ms;
  }

  void update(State desired_state, millis_t current_millis) {
    // Do cooldown if relay has been open too long.
    if (state == ON && ((current_millis - open_since) > open_before_cooldown_ms))
    {
      log("%s: Triggering cooldown! current_millis: %lu  open_since: %lu  diff:  %lu  open_before_cooldown_ms:  %lu", label,
          current_millis, open_since, current_millis - open_since, open_before_cooldown_ms);
      cooldown(current_millis);
      return;
    }

    if (desired_state == state || current_millis < next_update)
      return;

    switch (desired_state) {
      case ON:
        open();
        next_update = current_millis + change_threshold_ms;
        break;
      case OFF:
        close();
        next_update = current_millis + change_threshold_ms;
        break;
      case COOLDOWN:
        cooldown(current_millis);
        break;
      default:
        break;
    }
  }
};


}
#endif

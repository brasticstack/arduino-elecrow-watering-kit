#ifndef _WATERING_KIT_UI_H
#define _WATERING_KIT_UI_H

#include "U8glib.h"
#include "RTClib.h"
#include "common.h"

namespace WateringKitUI {

char daysOfTheWeek[7][5] = {"Sun", "Mon", "Tues", "Wed", "Thur", "Fri", "Sat",};

// good flower
unsigned char bitmap_good[] U8G_PROGMEM = {
  0x00, 0x42, 0x4C, 0x00, 0x00, 0xE6, 0x6E, 0x00, 0x00, 0xAE, 0x7B, 0x00, 0x00, 0x3A, 0x51, 0x00,
  0x00, 0x12, 0x40, 0x00, 0x00, 0x02, 0x40, 0x00, 0x00, 0x06, 0x40, 0x00, 0x00, 0x06, 0x40, 0x00,
  0x00, 0x04, 0x60, 0x00, 0x00, 0x0C, 0x20, 0x00, 0x00, 0x08, 0x30, 0x00, 0x00, 0x18, 0x18, 0x00,
  0x00, 0xE0, 0x0F, 0x00, 0x00, 0x80, 0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x01, 0x00,
  0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x02, 0xC1, 0x00, 0x00, 0x0E, 0x61, 0x00,
  0x00, 0x1C, 0x79, 0x00, 0x00, 0x34, 0x29, 0x00, 0x00, 0x28, 0x35, 0x00, 0x00, 0x48, 0x17, 0x00,
  0x00, 0xD8, 0x1B, 0x00, 0x00, 0x90, 0x1B, 0x00, 0x00, 0xB0, 0x09, 0x00, 0x00, 0xA0, 0x05, 0x00,
  0x00, 0xE0, 0x07, 0x00, 0x00, 0xC0, 0x03, 0x00
};

// bad flower
unsigned char bitmap_bad[] U8G_PROGMEM = {
  0x00, 0x80, 0x00, 0x00, 0x00, 0xC0, 0x00, 0x00, 0x00, 0xE0, 0x0D, 0x00, 0x00, 0xA0, 0x0F, 0x00,
  0x00, 0x20, 0x69, 0x00, 0x00, 0x10, 0x78, 0x02, 0x00, 0x10, 0xC0, 0x03, 0x00, 0x10, 0xC0, 0x03,
  0x00, 0x10, 0x00, 0x01, 0x00, 0x10, 0x80, 0x00, 0x00, 0x10, 0xC0, 0x00, 0x00, 0x30, 0x60, 0x00,
  0x00, 0x60, 0x30, 0x00, 0x00, 0xC0, 0x1F, 0x00, 0x00, 0x60, 0x07, 0x00, 0x00, 0x60, 0x00, 0x00,
  0x00, 0x60, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00,
  0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0xC7, 0x1C, 0x00,
  0x80, 0x68, 0x66, 0x00, 0xC0, 0x33, 0x7B, 0x00, 0x40, 0xB6, 0x4D, 0x00, 0x00, 0xE8, 0x06, 0x00,
  0x00, 0xF0, 0x03, 0x00, 0x00, 0xE0, 0x00, 0x00
};

const int L_GUTTER = 2;

enum UIMode {
  DISPLAY_SENSORS = 0,
  DISPLAY_TIME,
  SET_THRESHOLDS
};

struct UI {
    //const static millis_t REFRESH_INTERVAL_MS = 250;
    millis_t REFRESH_INTERVAL_MS = 250;
    U8GLIB_SSD1306_128X64* u8g = NULL;

    UI(U8GLIB_SSD1306_128X64* u8g_obj) {
      u8g = u8g_obj;
    }

    virtual ~UI() {}

    virtual void _do_activate() = 0;
    virtual void _do_update(millis_t current_time) = 0;
    virtual void deactivate() = 0;
    virtual void handlePress() = 0;
    virtual void handleLongPress() = 0;
    // TODO: Remove this proxy when init() is no longer called.
    virtual void init() {
      activate();
    }

    virtual void update(millis_t current_time) {
      if (current_time < next_update)
        return;

      if (!u8g)
        return;

      _do_update(current_time);

      next_update = current_time + REFRESH_INTERVAL_MS;
    }

    void activate() {
      next_update = 0; // Update ASAP.
      _do_activate();
    }

  protected:
    millis_t next_update = 0;
};

struct DisplaySensorUI : public UI {
    //const static millis_t REFRESH_INTERVAL_MS = 50;
    char sensed_percentage[5] = {""};

    DisplaySensorUI(U8GLIB_SSD1306_128X64* u8g)  : UI(u8g) {
      REFRESH_INTERVAL_MS = 50;
    }
    virtual ~DisplaySensorUI() {}

    virtual void _do_activate() {
      log("SensorUI activate");
      u8g->setFontPosBottom();
      u8g->setFont(u8g_font_7x14);
      u8g->setFontRefHeightText();
      ln_height = u8g->getFontAscent() - u8g->getFontDescent();
    }

    virtual void _do_update(millis_t current_time) {
      //log("SensorUI update");

      u8g->firstPage();
      do
      {
        drawTH();
        drawflowers();
      } while (u8g->nextPage());

    }

    virtual void deactivate() {}
    virtual void handlePress() {};
    virtual void handleLongPress() {};

    void drawflowers(void)
    {
      for (int i = 0; i < NUM_SENSORS; ++i)
        u8g->drawXBMP(i * 32, 0, 32, 30, (sensor_values[i] < low_thresholds[i]) ? bitmap_bad : bitmap_good);
    }

    void drawMoistureVal(int x, int y, int val, char* label)
    {
      snprintf(sensed_percentage, 5, "%3d%%", val);
      u8g->drawStr(x, y + ln_height + 2, label);
      u8g->drawStr(x, y, sensed_percentage);
    }

    void drawTH(void)
    {
      for (int i = 0; i < NUM_SENSORS; ++i)
        drawMoistureVal(L_GUTTER + (i * 32), 45, sensor_values[i], sensor_labels[i]);
    }

  protected:
    u8g_uint_t ln_height = 0;
};

struct DisplayTimeUI : public UI {
  RTC_DS1307 RTC;
  static const size_t DATETIME_LEN = 20;
  const char DATETIME_FMT[DATETIME_LEN] = "YYYY-MM-DD hh:mm:ss";
  char datetime_str[DATETIME_LEN] = "0000/00/00 00:00:00";
  bool is_complete = false;

  DisplayTimeUI(U8GLIB_SSD1306_128X64* u8g) : UI(u8g) {
    RTC.begin();
  }
  virtual ~DisplayTimeUI() {}

  virtual void _do_activate() {
    log("TimeUI activate");
    is_complete = false;
    u8g->setFont(u8g_font_6x10);
  }

  virtual void _do_update(millis_t current_time) {
    //log("TimeUI update");
    u8g->firstPage();
    do
    {
      drawtime();
      u8g->drawStr(L_GUTTER, 55 , "Pugh's plants");
    } while (u8g->nextPage());
  }

  virtual void deactivate() {}
  virtual void handlePress() { is_complete = true; }
  virtual void handleLongPress() {}

  void drawtime(void)
  {
    if (!RTC.isrunning())
    {
      u8g->drawStr(L_GUTTER, 20, "RTC is NOT running!");
      RTC.adjust(DateTime(__DATE__, __TIME__));
    }
    else
    {
      DateTime now = RTC.now();
      u8g->setFont(u8g_font_6x10);
      // RTClib.DateTime is weird, and its toString requires your dest contain the string format, which
      // it replaces with the formatted string.
      // TODO: Replace RTClib with newer version that has toString()!
      // see: https://adafruit.github.io/RTClib/html/class_date_time.html#a3753bf2ae254315a56f7ee38ff0b188b
      // strncpy(datetime_str, DATETIME_FMT, DATETIME_LEN);
      // now.toString(datetime_str);
      // END TODO
      snprintf(datetime_str, DATETIME_LEN, "%d/%02d/%02d %d:%02d:%02d", now.year(), now.month(),
               now.day(), now.hour(), now.minute(), now.second());
      u8g->drawStr(L_GUTTER, 11, datetime_str);
      u8g->drawStr(L_GUTTER, 22, daysOfTheWeek[now.dayOfTheWeek()]);
    }
  }
};

struct ThresholdUI : public UI {
    enum ELEMENT {
      PREV,
      LOW_T,
      HI_T,
      NEXT,
    };
    const static millis_t REFRESH_INTERVAL_MS = 100;
    const static size_t PLANT_LABEL_LEN = 16;
    const static size_t THRESHOLD_LEN = 10;
    ELEMENT current_element = LOW_T;
    byte current_page = 0;
    bool is_complete = false;
    byte backup_low[NUM_SENSORS];
    byte backup_hi[NUM_SENSORS];

    char plant_label[PLANT_LABEL_LEN] = {""};
    char low_threshold_label[THRESHOLD_LEN] = {""};
    char hi_threshold_label[THRESHOLD_LEN] = {""};

    ThresholdUI(U8GLIB_SSD1306_128X64* u8g) : UI(u8g) { }
    virtual ~ThresholdUI() {}

    virtual void backup_current() {
      memcpy(backup_low, low_thresholds, NUM_SENSORS);
      memcpy(backup_hi, hi_thresholds, NUM_SENSORS);
    }

    virtual void restore_current() {
      memcpy(low_thresholds, backup_low, NUM_SENSORS);
      memcpy(hi_thresholds, backup_hi, NUM_SENSORS);
    }

    virtual void _do_activate() {
      log("thresholdUI activate");
      is_complete = false;
      backup_current();
      
      u8g->setFont(u8g_font_7x14);
      u8g->setFontPosTop();
      u8g->setFontRefHeightText();

      ln_height = u8g->getFontAscent() - u8g->getFontDescent();
      current_page = 0;
      current_element = 1;
    }

    virtual void _do_update(millis_t current_time) {
      u8g->firstPage();
      do
      {
        // Print page number label.
        snprintf(plant_label, PLANT_LABEL_LEN, "Plant %d: %d%%", current_page + 1, sensor_values[current_page]);
        snprintf(low_threshold_label, THRESHOLD_LEN, "Low: %d", low_thresholds[current_page]);
        snprintf(hi_threshold_label, THRESHOLD_LEN, "HI: %d", hi_thresholds[current_page]);

        u8g->setDefaultForegroundColor();

        if ( current_element == PREV )
          draw_inverse(L_GUTTER, 0, (current_page > 0) ? "<" : "< Cancel");
        else
          u8g->drawStr(L_GUTTER, 0, (current_page > 0) ? "<" : "< Cancel");

        u8g->drawStr(L_GUTTER, ln_height, plant_label);

        if ( current_element == LOW_T )
          draw_inverse(L_GUTTER, 2 * ln_height, low_threshold_label);
        else
          u8g->drawStr(L_GUTTER, 2 * ln_height, low_threshold_label);

        if ( current_element == HI_T )
          draw_inverse(L_GUTTER, 3 * ln_height, hi_threshold_label);
        else
          u8g->drawStr(L_GUTTER, 3 * ln_height, hi_threshold_label);

        if ( current_element == NEXT )
          draw_inverse(L_GUTTER, 4 * ln_height, (current_page < NUM_SENSORS - 1) ? ">" : "> Accept");
        else
          u8g->drawStr(L_GUTTER, 4 * ln_height, (current_page < NUM_SENSORS - 1) ? ">" : "> Accept");
      } while (u8g->nextPage());
    }

    virtual void deactivate() { cancel(); }

    virtual void cancel() {
      log("Canceling.");
      restore_current();
      is_complete = true;
    }

    virtual void save() {
      is_complete = true;
      if(save_callback)
        save_callback();
    }

    virtual void handlePress() {
      switch (current_element) {
        case PREV:
          if ( current_page == 0)
          {
            cancel();
            return;
          }
          current_page = (current_page == 0) ? NUM_SENSORS : current_page - 1;
          current_element = LOW_T;
          break;
        case NEXT:
          if (current_page == NUM_SENSORS - 1 && current_element == NEXT) {
            save();
            return;
          }
          current_page = (current_page == NUM_SENSORS - 1) ? 0 : current_page + 1;
          current_element = LOW_T;
          break;
        case LOW_T:
          low_thresholds[current_page] = (low_thresholds[current_page] < 100) ? low_thresholds[current_page] + 1 : 0;
          break;
        case HI_T:
          hi_thresholds[current_page] = (hi_thresholds[current_page] < 100) ? hi_thresholds[current_page] + 1 : 0;
          break;
        default:
          break;
      };
      next_update = 0;
    }

    virtual void handleLongPress() {
      // Move to the next element.
      current_element = (current_element == NEXT) ? PREV : current_element + 1;
      
      next_update = 0;
    }

    void draw_inverse(byte x, byte y, const char* str)
    {
      u8g_uint_t width = u8g->getStrWidth(str);
      u8g->setDefaultForegroundColor();
      u8g->drawBox(x, y, width, ln_height);
      u8g->setDefaultBackgroundColor();
      u8g->drawStr(x, y, str);
      u8g->setDefaultForegroundColor();
    }

    void setSaveCallback(void(*cb)(void)) {
      save_callback = cb;
    }

  protected:
    u8g_uint_t ln_height = 0;
    void(*save_callback)(void) = NULL;
};

struct WateringKitUI {
  UIMode mode;
  U8GLIB_SSD1306_128X64 u8g;
  DisplaySensorUI sensorUI;
  DisplayTimeUI timeUI;
  ThresholdUI thresholdUI;
  UI* active = NULL;

  millis_t reset_time = 0;
  static const millis_t RESET_TIMEOUT = 5 * SECOND;

  WateringKitUI() : u8g(U8G_I2C_OPT_NONE), sensorUI(&u8g), timeUI(&u8g), thresholdUI(&u8g) {
  }

  void set_mode(UIMode new_mode) {
    if ( mode == new_mode )
      return;

    switch (new_mode) {
      case DISPLAY_SENSORS:
        active = dynamic_cast<UI*>(&sensorUI);
        break;
      case DISPLAY_TIME:
        active = dynamic_cast<UI*>(&timeUI);
        break;
      case SET_THRESHOLDS:
        active = dynamic_cast<UI*>(&thresholdUI);
        break;
    }
    if(active) { active->activate(); }
    mode = new_mode;
  }

  void update() {
    if (!active)
    {
      mode = -1; // So set_mode sees a change.
      set_mode(DISPLAY_SENSORS);
    }
    millis_t current_time = millis();
    
    if (mode == DISPLAY_TIME && (current_time > reset_time || timeUI.is_complete))
      set_mode(DISPLAY_SENSORS);
    else if (mode == SET_THRESHOLDS && thresholdUI.is_complete)
      set_mode(DISPLAY_SENSORS);

    if(active) { active->update(current_time); }
  }

  void handlePress(void) {
    log("UI HandlePress");
    if (mode == DISPLAY_SENSORS) {
      log("Setting mode to DISPLAY_TIME");
      set_mode(DISPLAY_TIME);
      reset_time = millis() + RESET_TIMEOUT;
      return;
    }
    else
      if(active) { active->handlePress(); }
  }

  void handleLongPress(void) {
    log("UI HandleLongPress");
    if (mode != SET_THRESHOLDS) {
      set_mode(SET_THRESHOLDS);
      return;
    }
    //thresholdUI.handleLongPress();
    active->handleLongPress();
  }
};

};

#endif

#include "EEPROM.h"

#include "common.h"
#include "CooldownRelay.h"
#include "LongPressableButton.h"
#include "WateringKitUI.h"

#define DEBUG_SENSOR false

// Fulfill externs declared in common.h; Allocate shared data here.
byte low_thresholds[NUM_SENSORS] = {DEFAULT_LOW_THRESHOLD, DEFAULT_LOW_THRESHOLD, DEFAULT_LOW_THRESHOLD, DEFAULT_LOW_THRESHOLD};
byte hi_thresholds[NUM_SENSORS] = {DEFAULT_HI_THRESHOLD, DEFAULT_HI_THRESHOLD, DEFAULT_HI_THRESHOLD, DEFAULT_HI_THRESHOLD};
int sensors[NUM_SENSORS] = {A0, A1, A2, A3};
byte sensor_values[NUM_SENSORS] = {0, 0, 0, 0};
char sensor_labels[NUM_SENSORS][3] = {"1", "2", "3", "4"};
//

byte valve_pins[NUM_SENSORS] = {6, 8, 9, 10};
WateringKitUI::WateringKitUI* ui = NULL;
CooldownRelay::CooldownRelay* valves[NUM_SENSORS];
CooldownRelay::CooldownRelay* pump;
LongPressableButton::LongPressableButton* button;

millis_t current = 0;
millis_t next_sense_debug = 0;

void ui_handlePress() {
  if (!ui)
  {
    Serial.println("No UI defined!");
    return;
  }
  ui->handlePress();
}

void ui_handleLongPress() {
  if (!ui)
  {
    Serial.println("No UI defined!");
    return;
  }

  ui->handleLongPress();
}

void make_thresholds_valid(byte& low, byte& hi) {
  byte tmp = 0;

  // If unset, set to defaults.
  if ( low == DEFAULT_EEPROM_VAL )
    low = DEFAULT_LOW_THRESHOLD;
  if ( hi == DEFAULT_EEPROM_VAL )
    hi = DEFAULT_HI_THRESHOLD;

  // Clamp to 100 max.
  low = min(low, 100);
  hi = min(hi, 100);

  // Hi has to be greater than low.
  if ( low > hi ) {
    tmp = hi;
    hi = low;
    low = tmp;
  }

  // Make sure there's enough difference between the two thresholds
  // to avoid flapping.
  if (hi - low < MIN_THRESHOLD_DIFFERENCE) {
    if ( hi > MIN_THRESHOLD_DIFFERENCE )
    {
      low = hi - MIN_THRESHOLD_DIFFERENCE;
    }
    else
    {
      hi = MIN_THRESHOLD_DIFFERENCE;
    }
  }
}

void read_thresholds_from_eeprom()
{
  byte low = 0;
  byte hi = 0;

  for (size_t i = 0; i < NUM_SENSORS; ++i) {
    low = EEPROM.read(THRESHOLD_STORAGE_ADDR + i);
    hi = EEPROM.read(THRESHOLD_STORAGE_ADDR + NUM_SENSORS + i);

    log("Sensor: %d  Load from EEPROM[%d] Low: %d  Hi: %d", i, THRESHOLD_STORAGE_ADDR + i, low, hi);

    make_thresholds_valid(low, hi);

    log("After make_thresholds_valid: Low: %d  Hi %d", low, hi);
    low_thresholds[i] = low;
    hi_thresholds[i] = hi;
  }
}

void write_thresholds_to_eeprom() {
  for (size_t i = 0; i < NUM_SENSORS; ++i) {
    EEPROM.update(THRESHOLD_STORAGE_ADDR + i, low_thresholds[i]);
    EEPROM.update(THRESHOLD_STORAGE_ADDR + NUM_SENSORS + i, hi_thresholds[i]);
  }
}

void setup()
{
  Serial.begin(9600);
  #if DEBUG
  delay(2000);
  #endif
  read_thresholds_from_eeprom();

  // Create CooldownRelay objects, which declares their pins too.
  for (int i = 0; i < NUM_SENSORS; ++i) {
    char vlabel[10] = "";
    snprintf(vlabel, 10, "Valve %d", i);
    valves[i] = new CooldownRelay::CooldownRelay(valve_pins[i], vlabel, 2 * SECOND,       15 * SECOND);
  }
  // Set up CooldownRelay for pump.
  pump = new CooldownRelay::CooldownRelay(PUMP, "Pump", 2 * SECOND, 3 * MINUTE);
  // Set up LongPressableButton for switch.
  button = new LongPressableButton::LongPressableButton(BUTTON);

  // Set up UI.
  ui = new WateringKitUI::WateringKitUI();
  ui->thresholdUI.setSaveCallback(&write_thresholds_to_eeprom);

  //// Configure button callbacks.
  button->setPressCallback(&ui_handlePress);
  button->setLongPressCallback(&ui_handleLongPress);
}

void loop()
{
  // read the value from the moisture sensors:
  sense();
  current = millis();
  update_valves(current);
  update_pump(current);
  ui->update();
}

void sense(void)
{
  for (int i = 0; i < NUM_SENSORS; ++i) {
    sensor_values[i] = map(analogRead(sensors[i]), 600, 360, 0, 100);
    // Clamp value to 0 - 100.
    sensor_values[i] = constrain(sensor_values[i], 0, 100);

#if DEBUG_SENSOR
    if (current > next_sense_debug) {
      log("Sensor %d: %d", i, sensor_values[i]);
      if (i == NUM_SENSORS - 1) {
        next_sense_debug = current + 250;
      }
    }
#endif
  }
  button->update();
}

bool should_pump(void) {
  for (int i = 0; i < NUM_SENSORS; ++i) {
    if (valves[i]->is_open()) {
      return true;
    }
  }

  return false;
}

void update_pump(millis_t current_millis) {
  pump->update(should_pump() ? CooldownRelay::ON : CooldownRelay::OFF, current_millis);
}

void update_valves(millis_t current_millis)
{
  CooldownRelay::State set_state = CooldownRelay::OFF;

  for (int i = 0; i < NUM_SENSORS; ++i) {
    if (CooldownRelay::COOLDOWN == pump->state)
      // Make sure relays are off if pump is on cooldown.
      set_state = CooldownRelay::OFF;
    else if (sensor_values[i] < low_thresholds[i])
      set_state = CooldownRelay::ON;
    else if (sensor_values[i] > hi_thresholds[i])
      set_state = CooldownRelay::OFF;

    valves[i]->update(set_state, current_millis);
  }
}

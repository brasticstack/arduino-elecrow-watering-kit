#ifndef _WATERING_KIT_COMMON_H
#define _WATERING_KIT_COMMON_H

// Set up Serial for debugging, if DEBUG set to true here.
#define DEBUG false
#define Serial if(DEBUG) Serial

// Type for timestamps based on milliseconds.
typedef unsigned long millis_t;

// Time constants in millis.
const unsigned short SECOND = 1000;
const unsigned short MINUTE = 60 * SECOND;

// Defaults
const byte DEFAULT_LOW_THRESHOLD = 30;
const byte DEFAULT_HI_THRESHOLD = 55;
const byte MIN_THRESHOLD_DIFFERENCE = 10;
const size_t NUM_SENSORS = 4;
const int PUMP = 4;
const int BUTTON = 12;
const unsigned int THRESHOLD_STORAGE_ADDR = 0;
const byte DEFAULT_EEPROM_VAL = 0xFF;

// Declarations for shared global data.
extern byte low_thresholds[NUM_SENSORS];
extern byte hi_thresholds[NUM_SENSORS];
extern int sensors[NUM_SENSORS];
extern char sensor_labels[NUM_SENSORS][3];
extern byte sensor_values[NUM_SENSORS];

// Debug string buffer.
const size_t DEBUG_LEN = 128;
char debug_buf[DEBUG_LEN] = {0};

#if DEBUG
void log(const char* fmt, ...) {
  va_list args;
  va_start(args, fmt);
  vsnprintf(debug_buf, DEBUG_LEN, fmt, args);
  va_end(args);
  Serial.println(debug_buf);
}
#else
void log(const char* fmt, ...) {
}
#endif

#endif // _WATERING_KIT_COMMON_H

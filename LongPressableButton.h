#ifndef _LONG_PRESSABLE_BUTTON_H
#define _LONG_PRESSABLE_BUTTON_H

#include "common.h"

namespace LongPressableButton {

const millis_t DEFAULT_DEBOUNCE_MS = 0;
const millis_t DEFAULT_LONG_PRESS_MS = 0.6 * SECOND;

enum ButtonState {
  UNPRESSED = 0,
  PRESSED,
  LONG_PRESSED
};

struct LongPressableButton {
    millis_t debounce_ms = DEFAULT_DEBOUNCE_MS;
    millis_t long_press_ms = DEFAULT_LONG_PRESS_MS;
    millis_t double_press_ms = DEFAULT_LONG_PRESS_MS;
    byte pin = -1;
    byte pin_mode = INPUT;
    millis_t press_started = 0;
    ButtonState state = UNPRESSED;

    LongPressableButton(byte pin, byte pin_mode = INPUT, millis_t debounce_ms = DEFAULT_DEBOUNCE_MS, millis_t long_press_ms = DEFAULT_LONG_PRESS_MS) {
      this->pin = pin;
      this->debounce_ms = debounce_ms;
      this->long_press_ms = long_press_ms;
      this->pin_mode = (pin_mode != OUTPUT) ? pin_mode : INPUT;  // Allow INPUT or INPUT_PULLUP. Don't allow OUTPUT.
      this->_pressed_value = (this->pin_mode == INPUT) ? LOW : HIGH;

      pinMode(this->pin, this->pin_mode);
    }

    void setPressCallback(void(*cb)(void)) {
      press_callback = cb;
    }

    void setLongPressCallback(void(*cb)(void)) {
      long_press_callback = cb;
    }

    void setDoublePressCallback(void(*cb)(void)) {
      double_press_callback = cb;
    }

    void update() {
      bool pressed = digitalRead(pin) == _pressed_value;

      if ( pressed )
      {
        millis_t current = millis();
        if ( state == UNPRESSED ) {
          if( 0 == debounce_ms )
          {
            state = PRESSED;
            press_started = current;
            log("Button press!");
          }
          else {
            // Press initiated or bouncing.
            // press_started will be zero if we haven't seen a press initiated w/in the debounce period.
            if (press_started == 0)
            {
              Serial.println("Button press started");
              press_started = current;
            }
            else if (current - press_started >= debounce_ms) {
              // This is a press.
              Serial.println("Button press debounce threshold passed. Button is considered pressed.");
              state = PRESSED;
            }
          }
        }
        else if (state == PRESSED && current - press_started >= long_press_ms) {
          Serial.println("LONG PRESS");
          // It's a long press!
          state = LONG_PRESSED;
          if(long_press_callback)
          {
            Serial.println("Calling long press callback.");
            long_press_callback();
          }
        }

      }
      else {
        // Unpressed recently.
        if (state != UNPRESSED)
        {
          Serial.println("Button released.");
          if (state == PRESSED)
          {
            // The button was just released.
            if(press_callback)
              press_callback();
          }
          // LONG_PRESS callback happens while button is still pressed. It happened above.
          
          state = UNPRESSED;
          press_started = 0;
        }
      }
    }

  protected:
    byte _pressed_value = LOW;
    void(*press_callback)(void) = NULL;
    void(*long_press_callback)(void) = NULL;
    void(*double_press_callback)(void) = NULL;

}; // end struct LongPressableButton

}; // End namespace


#endif  // Include guard.
